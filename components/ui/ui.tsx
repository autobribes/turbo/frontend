import { MouseEventHandler, useCallback } from "react";

type ButtonProps = {
  className?: string;
  disabled?: boolean;
  children?: React.ReactNode;
  onClick?: () => any;
};

export const Button = ({
  className,
  disabled,
  children,
  onClick,
}: ButtonProps) => (
  <button
    disabled={disabled}
    onClick={onClick}
    className={`flex flex-col justify-center items-center p-3 rounded-md text-3xl transition ease-in-out duration-300 hover:enabled:scale-[1.02] hover:enabled:bg-blue-600 bg-blue-700 ${
      className ? className : ""
    } ${
      disabled
        ? "bg-slate-900 opacity-80 text-gray-700"
        : "bg-blue-700 opacity-100 text-white"
    }`}
  >
    {children}
  </button>
);

export const SwitchButton = ({
  className,
  disabled,
  children,
  onClick,
}: ButtonProps) => (
  <button
    disabled={disabled}
    onClick={onClick}
    className={`flex flex-col h-16 w-full justify-center items-center px-3 py-2 rounded-md text-3xl transition ease-in-out duration-300 hover:enabled:scale-[1.02] hover:enabled:bg-blue-700 hover:enabled:text-white ${
      className ? className : ""
    } ${
      !disabled
        ? "bg-slate-900 opacity-80 text-gray-700"
        : "bg-blue-700 opacity-100 text-white"
    }`}
  >
    {children}
  </button>
);

type ModalProps = {
  children?: React.ReactNode;
  isOpen: boolean;
  onClose?: () => any;
  className?: string;
};

export const Modal = ({ children, isOpen, onClose, className }: ModalProps) => {
  const close: MouseEventHandler<HTMLDivElement> = (e) => {
    const element = e.target as Element;
    if (
      element.matches(".modal-content") ||
      element.matches(".modal-content *")
    ) {
      return;
    }
    onClose && onClose();
  };
  const onClick = useCallback(close, [onClose]);

  return isOpen ? (
    <div
      className={`fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-black bg-opacity-60 flex flex-col items-center justify-center backdrop-blur-[16px] transition-opacity`}
    >
      {onClose !== undefined ? (
        <div
          className="fa fa-times-circle cursor-pointer text-3xl"
          onClick={onClick}
        />
      ) : (
        <></>
      )}
      <div
        className={`modal-content p-[24px] text-white cursor-auto flex flex-col gap-2 ${className}`}
      >
        {children}
      </div>
    </div>
  ) : (
    <></>
  );
};

type ConfirmModalProps = {
  children?: React.ReactNode;
  buttonTitle: string;
  isOpen: boolean;
  onConfirm: () => any;
};

export const ConfirmModal = ({
  children,
  buttonTitle,
  isOpen,
  onConfirm,
}: ConfirmModalProps) => {
  const onClick = useCallback(() => {
    onConfirm();
  }, [onConfirm]);

  return (
    <Modal isOpen={isOpen}>
      {children}
      <Button onClick={onClick}>{buttonTitle}</Button>
    </Modal>
  );
};

type BlockProps = {
  children?: React.ReactNode;
  className?: string;
};

export const Block = ({ children, className }: BlockProps) => (
  <div
    className={`flex flex-col h-full w-full items-center p-6 bg-no-repeat bg-cover bg-center gap-4 z-30 ${
      className ? className : ""
    }`}
  >
    {children}
  </div>
);

type ExternalLinkProps = {
  href: string;
  className?: string;
  children: React.ReactNode;
};

export const ExternalLink = ({
  href,
  className,
  children,
}: ExternalLinkProps) => (
  <a
    href={href}
    rel="noreferrer"
    target="_blank"
    className={`underline ${className}`}
  >
    {children}
  </a>
);
