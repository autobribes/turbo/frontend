import { ConnectButton } from "@rainbow-me/rainbowkit";
import type { NextPage } from "next";
import Head from "next/head";
import { PropsWithChildren, useEffect } from "react";
import Image from "next/image";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ReliquaryUI } from "components/reliquary";
import { useIsMounted } from "utils/utils";
import { ConfirmModal, ExternalLink } from "components/ui/ui";
import Link from "next/link";
import { useLocalStorage } from "usehooks-ts";

export const Home: NextPage = () => {
  const isMounted = useIsMounted();

  if (!isMounted) return null; // To prevent SSR matching issues
  return (
    <div>
      <div className="text-xl bg-red-300 p-4 text-center">
        Rewards have been paused on Turbo while we build version 2 of the
        protocol. Please withdraw until then and join{" "}
        <ExternalLink href="https://discord.com/invite/SpfdM2VwUy">
          Optimism Prime{"'"}s discord
        </ExternalLink>{" "}
        for updates.
      </div>
      <Layout>
        <ReliquaryUI />
        <AuditWarning />
      </Layout>
    </div>
  );
};

function AuditWarning() {
  const [warningAknowledged, setWarningAknowledged] = useLocalStorage(
    "turbo_autobribes-warning-aknowledged",
    false,
  );
  const [warningAknowledgedTimestamp, setWarningAknowledgedTimestamp] =
    useLocalStorage("turbo_autobribes-warning-aknowledged-timestamp", 0);
  useEffect(() => {
    const oneWeek = 1000 * 3600 * 24 * 7;
    if (Date.now() - warningAknowledgedTimestamp > oneWeek) {
      setWarningAknowledged(false);
    }
  }, [warningAknowledgedTimestamp, setWarningAknowledged]);

  return (
    <>
      {!warningAknowledged ? (
        <ConfirmModal
          isOpen={!warningAknowledged}
          onConfirm={() => {
            setWarningAknowledged(true);
            setWarningAknowledgedTimestamp(Date.now());
          }}
          buttonTitle="Accept"
        >
          <div className="flex flex-col text-center text-xl gap-2">
            <p>
              <i className="fa fa-2x fa-exclamation-triangle fa-beat"></i>{" "}
              DISCLAIMER{" "}
              <i className="fa fa-2x fa-exclamation-triangle fa-beat"></i>
            </p>
            <p>
              The Autobribes protocol code is unaudited, use at your own risk.
            </p>
          </div>
        </ConfirmModal>
      ) : (
        <></>
      )}
    </>
  );
}

function Layout({ children }: PropsWithChildren) {
  return (
    <div className="bg-black text-white min-h-screen flex flex-col">
      <Head>
        <title>Turbo - Autobribes for Retro</title>
      </Head>
      <HeaderBar />
      <main className="pl-16 pr-16 grow">{children}</main>
      <Footer />
      <ToastContainer theme="dark" />
    </div>
  );
}

function HeaderBar() {
  return (
    <header className="flex flex-row items-center justify-between pt-4 pr-8 pl-8 font-roboto-cond text-lg">
      <LogoAndTitle />
      <div className="flex flex-row items-center gap-8 right-0">
        <Link href="/" className="underline">
          Home
        </Link>
        <ExternalLink href="https://optimism-prime.notion.site/optimism-prime/Autobribes-Documentation-0f19f40f81d74cc08947de80b7013fbf">
          Documentation
        </ExternalLink>
        <ExternalLink
          href="https://retro.finance/"
          className="items-center flex flex-col"
        >
          <Image
            src="/retro/logo+title.webp"
            alt="logo"
            width={85}
            height={24}
            unoptimized={true}
            quality={100}
          />
        </ExternalLink>
        <ConnectButton />
      </div>
    </header>
  );
}

function LogoAndTitle() {
  return (
    <Link href="/">
      <div className="flex flex-row justify-center items-center text-3xl lg:text-5xl mt-2 mb-2">
        <Image
          src="/turbo-draft-light-logo.png"
          alt="logo"
          width={72}
          height={72}
          unoptimized={true}
          quality={100}
        />
        <div className="ml-2 flex flex-col">
          <span className="mr-[16px]">Turbo</span>
          <span className="mr-[10px] text-sm">Autobribes for Retro</span>
        </div>
      </div>
    </Link>
  );
}

function Footer() {
  return (
    <footer className="flex pb-8 justify-center items-center">
      <a
        href="https://optimismprime.io"
        rel="noopener noreferrer"
        target="_blank"
        className="flex"
      >
        Made by Builder Bots from
        <Image
          className="ml-2 mr-2"
          alt=""
          src="/opp-logo.png"
          width="24"
          height="24"
        />{" "}
        Optimism Prime
      </a>
    </footer>
  );
}

export default Home;
