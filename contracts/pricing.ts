import { Address, formatUnits, getAddress } from "viem";
import { PublicClient, useContractReads, usePublicClient } from "wagmi";
import { ActiveLiquidityManagerERC20 } from "./abi/ActiveLiquidityManagerERC20";
import { ADDRESSES } from "./constants";
import { UniswapV3Pool } from "./abi/UniswapV3Pool";
import { ERC20 } from "./abi/ERC20";
import { useQueries } from "@tanstack/react-query";
import univ3prices from "@thanpolas/univ3prices";
import { SECONDS_PER_YEAR } from "utils/constants";

export type TokenInfo = {
  address: Address;
  decimals: number;
  name: string;
  symbol: string;
  totalSupply: {
    formatted: string;
    value: bigint;
  };
};

export type ALMInfo = {
  liquidityPool: Address; // UniV3 liquidity pool on which the ALM executes its strategy
  token0: Address;
  token1: Address;
  totalAmounts: [bigint, bigint];
} & TokenInfo;

export type UseActiveLiquidityManagerInfoResult = {
  data?: { [key: Address]: ALMInfo }; // Map ALM token address to info
} & UseQueryResult;

export function useActiveLiquidityManagerInfo(
  almTokens: TokenInfo[],
): UseActiveLiquidityManagerInfoResult {
  const { data, ...queryResult } = useContractReads({
    allowFailure: false,
    contracts: almTokens
      .map(({ address }) => ({
        address,
        abi: ActiveLiquidityManagerERC20,
        functionName: "pool",
      }))
      .concat(
        almTokens.map(({ address }) => ({
          address,
          abi: ActiveLiquidityManagerERC20,
          functionName: "token0",
        })),
      )
      .concat(
        almTokens.map(({ address }) => ({
          address,
          abi: ActiveLiquidityManagerERC20,
          functionName: "token1",
        })),
      )
      .concat(
        almTokens.map(({ address }) => ({
          address,
          abi: ActiveLiquidityManagerERC20,
          functionName: "getTotalAmounts",
        })),
      ),
  });
  if (data === undefined) {
    return { ...queryResult };
  }
  return {
    ...queryResult,
    data: Object.fromEntries(
      almTokens.map((token, dataIdx) => {
        const liquidityPool = data[dataIdx] as Address;
        const token0 = data[almTokens.length + dataIdx] as Address;
        const token1 = data[2 * almTokens.length + dataIdx] as Address;
        const totalAmounts = data[3 * almTokens.length + dataIdx] as [
          bigint,
          bigint,
        ];

        return [
          token.address,
          {
            liquidityPool,
            token0,
            token1,
            totalAmounts,
            ...token,
          },
        ];
      }),
    ) as { [key: Address]: ALMInfo },
  };
}

export type TokenPrice = {
  formatted: string;
  value: number;
};

export async function fetchTokenPrice(
  publicClient: PublicClient,
  tokenAddress: Address,
): Promise<TokenPrice> {
  if (
    tokenAddress === ADDRESSES.polygon["USDC.e"] ||
    tokenAddress === ADDRESSES.polygon["USDT"]
  ) {
    return {
      formatted: `1.0`,
      value: 1,
    };
  }

  const pool = (() => {
    if (tokenAddress === ADDRESSES.polygon["WETH"]) {
      return ADDRESSES.polygon.univ3.pools["USDC.e-WETH"]["500"];
    }
    if (tokenAddress === ADDRESSES.polygon["WBTC"]) {
      return ADDRESSES.polygon.univ3.pools["WBTC-USDC.e"]["500"];
    }
    if (tokenAddress === ADDRESSES.polygon["WMATIC"]) {
      return ADDRESSES.polygon.univ3.pools["MATIC-USDC.e"]["500"];
    }
    return undefined;
  })();

  if (pool === undefined) {
    throw Error();
  }

  const [token0, token1, slot0] = await publicClient.multicall({
    allowFailure: false,
    contracts: [
      {
        address: pool,
        abi: UniswapV3Pool,
        functionName: "token0",
      },
      {
        address: pool,
        abi: UniswapV3Pool,
        functionName: "token1",
      },
      {
        address: pool,
        abi: UniswapV3Pool,
        functionName: "slot0",
      },
    ],
  });
  const [decimals0, decimals1] = await publicClient.multicall({
    allowFailure: false,
    contracts: [
      {
        address: token0,
        abi: ERC20,
        functionName: "decimals",
      },
      {
        address: token1,
        abi: ERC20,
        functionName: "decimals",
      },
    ],
  });

  const sqrtPriceX96 = slot0[0];
  let price = univ3prices(
    [decimals0, decimals1],
    sqrtPriceX96.toString(),
  ).toAuto({ reverse: tokenAddress === token0 }) as string;

  return {
    formatted: price,
    value: parseFloat(price),
  };
}

export function useTokenPrices(tokens: Address[]): {
  [key: Address]: TokenPrice | undefined;
} {
  const publicClient = usePublicClient();

  const queries = useQueries({
    queries: tokens.map((tokenAddress) => {
      return {
        queryKey: ["token-price", tokenAddress],
        queryFn: () => fetchTokenPrice(publicClient, tokenAddress),
      };
    }),
  });

  return Object.fromEntries(
    queries.map((result, resultIndex) => {
      return [tokens[resultIndex], result.isSuccess ? result.data : undefined];
    }),
  );
}

export function useTokenPrice(token?: Address): TokenPrice | undefined {
  const tokenPrices = useTokenPrices(token ? [token] : []);
  if (token === undefined) {
    return undefined;
  }
  return tokenPrices[token];
}

export type UniV3PoolPrice = {
  decimals0: number;
  decimals1: number;
  price: TokenPrice;
  reversePrice: TokenPrice;
};

export async function fetchUniV3PoolPrice(
  publicClient: PublicClient,
  pool: UniswapV3Pool,
): Promise<UniV3PoolPrice> {
  const [decimals0, decimals1] = await publicClient.multicall({
    allowFailure: false,
    contracts: [
      {
        address: pool.token0,
        abi: ERC20,
        functionName: "decimals",
      },
      {
        address: pool.token1,
        abi: ERC20,
        functionName: "decimals",
      },
    ],
  });

  const sqrtPriceX96 = pool.slot0.sqrtPriceX96;
  const price = univ3prices(
    [decimals0, decimals1],
    sqrtPriceX96.toString(),
  ).toAuto() as string;
  const reversePrice = univ3prices(
    [decimals0, decimals1],
    sqrtPriceX96.toString(),
  ).toAuto({ reverse: true }) as string;

  return {
    decimals0,
    decimals1,
    price: {
      formatted: price,
      value: parseFloat(price),
    },
    reversePrice: {
      formatted: reversePrice,
      value: parseFloat(reversePrice),
    },
  };
}

export function useUniV3PoolPrices(
  pools: { [key: Address]: UniswapV3Pool } | undefined,
): { [key: Address]: UniV3PoolPrice | undefined } | undefined {
  const publicClient = usePublicClient({ chainId: 137 });
  const queries = useQueries({
    queries:
      pools === undefined
        ? []
        : Object.entries(pools).map(([poolAddress, pool]) => {
            return {
              queryKey: ["univ3pool-price", poolAddress],
              queryFn: () => fetchUniV3PoolPrice(publicClient, pool),
            };
          }),
  });

  if (pools === undefined) {
    return undefined;
  }

  return Object.fromEntries(
    Object.entries(pools).map(([poolAddress, pool], poolIndex) => {
      const result = queries[poolIndex];
      return [poolAddress, result.data];
    }),
  ) as { [key: Address]: UniV3PoolPrice | undefined };
}

export function useALMTokenPrices(almsInfo: { [key: Address]: ALMInfo }): {
  [key: Address]: TokenPrice | undefined;
} {
  const tokens = new Set(
    Object.values(almsInfo)
      .map((almInfo) => almInfo.token0)
      .concat(Object.values(almsInfo).map((almInfo) => almInfo.token1)),
  );
  const tokenPrices = useTokenPrices([...tokens]);
  const { data: pools } = useUniswapV3Pools(
    Object.values(almsInfo).map((almInfo) => almInfo.liquidityPool),
  );
  const poolPrices = useUniV3PoolPrices(pools);

  if (poolPrices === undefined) {
    return {};
  }

  const data = Object.fromEntries(
    Object.entries(almsInfo).map(([almTokenAddress, almInfo]) => {
      let price0 = tokenPrices[almInfo.token0];
      let price1 = tokenPrices[almInfo.token1];
      const poolPrice = poolPrices[almInfo.liquidityPool];

      if (
        poolPrice === undefined ||
        (price0 === undefined && price1 === undefined)
      ) {
        return [almTokenAddress, undefined];
      }

      if (price0 === undefined && price1 !== undefined) {
        // Recover price0 from the pool and price1
        const priceOfToken0WrtToken1 = poolPrice.reversePrice;
        const priceOfToken0InUsd = priceOfToken0WrtToken1.value * price1.value;
        price0 = {
          formatted: priceOfToken0InUsd.toFixed(4),
          value: priceOfToken0InUsd,
        };
      }
      if (price1 === undefined && price0 !== undefined) {
        // Recover price1 from the pool and price0
        const priceOfToken1WrtToken0 = poolPrice.price;
        const priceOfToken1InUsd = priceOfToken1WrtToken0.value * price0.value;
        price1 = {
          formatted: priceOfToken1InUsd.toFixed(4),
          value: priceOfToken1InUsd,
        };
      }
      const totalAlmValue =
        parseFloat(formatUnits(almInfo.totalAmounts[0], poolPrice.decimals0)) *
          price0!.value +
        parseFloat(formatUnits(almInfo.totalAmounts[1], poolPrice.decimals1)) *
          price1!.value;
      const almTokenPrice =
        totalAlmValue / parseFloat(almInfo.totalSupply.formatted);

      return [
        almTokenAddress,
        {
          formatted: almTokenPrice.toFixed(8),
          value: almTokenPrice,
        },
      ];
    }),
  ) as { [key: Address]: TokenPrice | undefined };

  return data;
}

export type UniswapV3PoolSlot0 = {
  sqrtPriceX96: bigint;
  tick: bigint;
  observationIndex: bigint;
  observationCardinality: bigint;
  observationCardinalityNext: bigint;
  feeProtocol: bigint;
  unlocked: boolean;
};

export type UniswapV3Pool = {
  address: Address;
  token0: Address;
  token1: Address;
  slot0: UniswapV3PoolSlot0;
};

export type UseUniswapV3PoolsResult = {
  data?: { [key: Address]: UniswapV3Pool };
} & UseQueryResult;

export function useUniswapV3Pools(
  addresses: Address[],
): UseUniswapV3PoolsResult {
  const { data, ...queryResult } = useContractReads({
    allowFailure: false,
    contracts: addresses
      .map((address) => ({
        address,
        abi: UniswapV3Pool,
        functionName: "token0",
      }))
      .concat(
        addresses.map((address) => ({
          address,
          abi: UniswapV3Pool,
          functionName: "token1",
        })),
      )
      .concat(
        addresses.map((address) => ({
          address,
          abi: UniswapV3Pool,
          functionName: "slot0",
        })),
      ),
  });

  if (data === undefined) {
    return { ...queryResult };
  }

  return {
    ...queryResult,
    data: Object.fromEntries(
      addresses.map((address, addressIndex) => {
        const slot0 = data[addresses.length * 2 + addressIndex] as [
          bigint,
          bigint,
          bigint,
          bigint,
          bigint,
          bigint,
          boolean,
        ];
        return [
          address,
          {
            address,
            token0: getAddress(data[addressIndex] as string),
            token1: getAddress(data[addresses.length + addressIndex] as string),
            slot0: {
              sqrtPriceX96: slot0[0],
              tick: slot0[1],
              observationIndex: slot0[2],
              observationCardinality: slot0[3],
              observationCardinalityNext: slot0[4],
              feeProtocol: slot0[5],
              unlocked: slot0[6],
            },
          },
        ];
      }),
    ) as { [key: Address]: UniswapV3Pool },
  };
}

export function computeApr(
  balanceUsd: number,
  rewardRateUsd: number,
  share: number,
) {
  return (100 * (SECONDS_PER_YEAR * rewardRateUsd * share)) / balanceUsd;
}
